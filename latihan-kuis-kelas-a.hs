-- Kuis Pemrograman Fungsional Kelas A


-- 1) Tentukan tipe dan buat definisi fungsi yang menerima tiga buah
-- bilangan bulat positif dan menghasilkan bilangan terbesar dari bilangan
-- input tersebut.
-- MaxTiga 4 3 5
-- 5

maxTiga :: Ord a => a -> a -> a -> a
-- follows max function

maxTiga x y z = max x (max y z)

-- alternative?



-- 2) Jelaskan proses evaluasi dari ekspresi berikut, berikan juga
-- hasil evaluasi ekspresinya
-- [ (x,y) | x <- [1..4], y <- [2..6], x*2 == y]

kuisno2 = [ (x,y) | x <- [1..4], y <- [2..6], x*2 == y]

-- Ekspresi beirkut akan mencari nilai x dan y dimana x*2 memiliki nilai
-- yang sama dengan y. Nilai x berada di range 1-4, dan nilai y berada
-- di range 2-6. Pertama diambil dulu nilai x pertama dalam
-- range, yaitu 1. x=1 kemudian akan dibandingkan dengan range y
-- dengan urutan (x=1, y=2), (x=1, y=3), dst. sampai range y habis.
-- Apabila range y sudah habis, nilai x pindah ke nilai x selanjutnya,
-- yaitu x=2. x=2 akan dibandingkan dengan y sama dengan proses x=1.

-- Evaluasi akan berhenti apabila range x dan y sudah mencapai akhir. Apabila
-- selama proses didapat pasangan x dan y yang cocok dengan ekspresi, maka
-- pasangan didaftarkan sebagai hasil.

-- Hasil ekspresi: [(1,2),(2,4),(3,6)]



-- 3) Definisikan fungsi quicksort, sebagaimana algoritma quick sort pada
-- literature

quickSort [] = []
quickSort (x:xs) = quickSort [y | y <- xs, y <= x] ++ [x] ++ quickSort [y | y <- xs, y > x]

-- alternative? could use hof but have to figure out a way.



-- 4) Buat definisi menggunakan foldr atau foldl untuk menjumlahkan
-- elemen dalam sebuah list
-- jumlahList [1,2,3,4]
-- 10

--foldr
jumlahListr [] = 0 -- covers case when list is empty
jumlahListr xs = foldr (+) 0 xs

--foldl
jumlahListl [] = 0 -- covers case when list is empty
jumlahListl xs = foldl (+) 0 xs

-- in some cases, foldr is more effective than foldl



-- 5) Pelajari fungsi misteri berikut ini, dan berikan langkah-langkah
-- evaluasi hingga mencapai hasilnya untuk input yang diberikan.

-- misteri xs ys = concat (map (\x -> map (\y -> (x,y)) ys) xs)

misteri xs ys = concat (map (\x -> map (\y -> (x,y)) ys) xs)

-- concat (map (\x -> map (\y -> (x,y)) ys) xs)

-- f = \x -> map (\y -> (x,y)) ys
-- concat (map f xs)

-- concat ( [f x | x <- xs] )

-- when x = [1,2,3], y = [4,5,6]
-- = concat ( [f1, f2, f3])

-- concat ([f1])
-- = (\x -> map (\y -> (x,y)) ys) 1
-- = map (\y -> (1,y)) ys
-- [(1,4), (1,5), (1,6)]

-- repeat process for f2 and f3.

-- End result = [(1,4),(1,5),(1,6),(2,4),(2,5),(2,6),(3,4),(3,5),(3,6)]



-- 6) Buatlah definisi primes yang menyatakan infinite list dari
-- bilangan prima menerapkan algoritma Sieve of Erastothenes 

primes = sieve [2..]
    where sieve (x:xs) = x : sieve [y | y<-xs, y `mod` x /= 0]


-- 7) Tentukan tipe dan fungsi flip sebagaimana diajarkan di
-- kelas/literatur

-- from ghci :t flip
flip' :: (a -> b -> c) -> b -> a -> c

flip' a b c = c `a` b



-- 9) definisi fungsi turnRight

-- turnRight :: Robot ()
-- turnRight = updateState (\s -> s {facing = right (facing s)})

-- 10) Jelaskan tipe dari fungsi toEnum dalam fungsi Right.

toEnum :: Int -> Direction

-- why? Karena toEnum merubah suatu integer menjadi suatu data
-- type lain, misal char. Pada kode Robot, toEnum mengubah integer
-- menjadi Direction, yang merupakan arah dari robot.
-- Directon bisa dilakukan operasi tersebut karena Direction derivative
-- dari Enum.