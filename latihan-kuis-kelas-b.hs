 -- Kuis Pemrograman Fungsional Kelas B


-- 1) Tentukan tipe dan buat definisi fungsi yang menerima dua buah
-- bilangan bulat positif dan menghasilkan KPK.
-- kpk 4 3
-- 12

-- using lcm function (cheat way hahaha)
kpk :: Integral a => a -> a -> a
kpk a b = lcm a b

-- not using lcm function

kpk2 :: Integral a => a -> a -> a
kpk2 a b = head [x | x <- [1 .. (a*b)] , x `mod` a == 0, x `mod` b == 0]



-- 2) Proses evaluasi dari
-- [ (x,y) | x <- [1..3], y <- [1..(2*x)]]

kuisno2 = [ (x,y) | x <- [1..3], y <- [1..(2*x)]]

-- ekspresi akan membuat semua kemungkinan pasangan x dan
-- y dari range x dan y yang sudah didefinisikan
-- proses akan dimulai dari x=1, dan kemudian akan berjalan
-- sesuai dengan range y, dengan urutan (x=1, y=1), (x=1, y=2).
-- Apabila range y sudah habis, nilai x akan berpindah ke selanjutnya
-- yaitu x=2. Proses yang terjadi di x=1 akan diaplikasikan ke x=2.

-- Evaluasi akan berhenti apabila range x dan y habis.

-- Hasil ekspresi: [(1,1),(1,2),(2,1),(2,2),(2,3),(2,4),(3,1),(3,2),(3,3),(3,4),(3,5),(3,6)]



-- 3) tipe dan definisi mergesort
-- ?



-- 4) gunakan foldr atau foldl untuk mengambil elemen paling besar
-- dalam sebuah list yang berisi bilangan bulat positif. Bila input
-- list kosong, jawabannya 0.
-- maxList [1,2,3,4]
-- 4

maxList [] = 0
maxList xs = foldr max 0 xs

-- alternative method, have to understand it fully tho
maxList1 :: (Ord a) => [a] -> a
maxList1 = foldr1 (\x acc -> if x > acc then x else acc)



-- nomor 5 idem prosesnya sama kelas a.



-- 6) Buatlah definisi pythagoras yang menyatakan infinite list 
-- dari triple bilangan bulat positif yang membentuk persamaan
-- pythagoras
-- take 2 pythagoras
-- [(3,4,5), (6,8,10)]

pythagoras = [ (x,y,z) | z <- [5..], y <- [1..z], x <- [1..y], x*x+y*y==z*z]
-- more alternatives can be found in lazy-evaluation.hs



-- nomor 7 idem prosesnya sama kelas a.
