import Data.List

-- Week 5: List Comprehension and Lazy Evaluation

-- 1. Uraikan langkah evaluasi dari ekspresi berikut
-- [ x+y | x <- [1 .. 4], y <- [2 .. 4], x > y ]

lev1 = [ x+y | x <- [1 .. 4], y <- [2 .. 4], x > y ]

-- Step 1: This expression will add the x and y values with
-- the condition that x is greater than y

-- x value ranges from 1-4, while the y value ranges from 2-4.

-- Evaluation will start by getting the first x value, which is 1.
-- x = 1 will then be compared with y values with the order
-- (x=1, y=2), (x=1, y=3), then (x=1, y=4). When the range of y is
-- iterated fully, the x value will change to the next one, which
-- is x = 2. The same process will then be applied.

-- Evaluation will stop when the x and y range is fully iterated.
-- If a value conforms to the requirements, then the pair of x and y
-- values will be registered as the result.

-- Expression Result : [5, 6, 7]



-- 2. Buatlah fungsi divisor yang menerima
-- sebuah bilangan bulat n dan mengembalikan
-- list bilangan bulat positif yang membagi habis n, Contoh:
-- LatihanLazy> divisor 12
-- [1,2,3,4,6,12]

divisor' n = [ x | x <- [1..n], n`mod`x == 0]

-- if expected result is descending ([12,6,4,3,2,1])
divisordesc n = [ x | x <- [n, n-1..1], n`mod`x == 0]



-- 3. Buatlah definisi quick sort menggunakan list comprehension.

quickSort [] = []
quickSort (x:xs) = quickSort [y | y <- xs, y <= x] ++ [x] ++ quickSort [y | y <- xs, y > x]



-- 4. Buatlah definisi infinite list untuk permutation.

perm [] = [[]]
perm ls = [ x:rem | x <- ls, rem <- perm (ls\\[x]) ]



-- 5. Buatlah definisi untuk memberikan infinite list dari
-- bilangan prima menerapkan
-- algoritma Sieve of Erastothenes.

primes = sieve[2..100]
    where
        sieve (x:xs) = x:(sieve[ y | y <- xs, y`mod`x /= 0])

-- gives warning: non-exhaustive patterns in function sieve.

-- alternative: ?



-- 6. Buatlah definisi infinite list dari triple pythagoras.
-- List tersebut terdiri dari element
-- triple bilangan bulat positif yang mengikut persamaan pythagoras x
-- 2 + y
-- 2 = z
-- 2
-- Contoh:
-- LatihanLazy > pythaTriple
-- [(3,4,5),(6,8,10),(5,12,13),(9,12,15),(8,15,17),(12,16,20) … ]
-- Perhatian urutan penyusun list comprehension nya,
-- coba mulai dari variable z!

pythaTriple1 = [ (x,y,z) | z <- [5..], y <- [1..z], x <- [1..z], x*x+y*y==z*z]
-- outputs correct result, but there are reoccurences.
-- Example: (4,3,5) and (3,4,5)

pythaTriple2 = [ (x,y,z) | z <- [5..], y <- [1..z], x <- [1..y], x*x+y*y==z*z]
-- most correct for now!

pythaTriple2rev = [ (x,y,z) | z <- [5..], y <- [z, z-1..1], x <- [y, y-1..1], x*x+y*y==z*z]
-- reversed y and x, but still outputs the same result.
-- question: is the running time same between pythaTriple2 and pythaTriple2rev?

pythaTriple4 = [ (x,y,z) | z <- [5..], y <- [1..z], x <- [1..z], y>x, x*x+y*y==z*z]
-- also works, but doesn't conform to the the question's requirements.