import Data.Maybe

-- List Comprehensions and Higher-Order Functions

-- Can you rewrite the following list comprehensions using the higher-order
-- functions map and filter? You might need the function concat too.

-- 1. [ x+1 | x <- xs ]

func1 xs = [ x+1 | x <- xs ]

hofunc1 xs = map (+1) xs


-- 2. [ x+y | x <- xs, y <-ys ]

func2 xs ys = [ x+y | x <- xs, y <- ys ]

hofunc2 xs ys = concat (map (\x -> map (\y -> x+y) ys) xs)


-- 3. [ x+2 | x <- xs, x > 3 ]

func3 xs = [ x+2 | x <- xs, x > 3 ]

hofunc3 xs = map (+2) (filter (>3) xs)

-- 4. [ x+3 | (x,_) <- xys ]

func4 xys = [ x+3 | (x,_) <- xys ]

hofunc4 xys = map (\(x,_) -> x+3) xys

-- 5. [ x+4 | (x,y) <- xys, x+y < 5 ]

func5 xys = [ x+4 | (x,y) <- xys, x+y < 5 ]

hofunc5 xys = map (\(x,_) -> x+4) (filter (\(x,y) -> x+y < 5) xys)

-- 6. [ x+5 | Just x <- mxs ]

func6 mxs = [ x+5 | Just x <- mxs ]

hofunc6 mxs = map (\(Just x) -> x+5) (filter isJust mxs)

-- Still don't really understand this one :(



-- Can you rewrite the following list comprehensions using the higher-order
-- functions map and filter? You might need the function concat too.

-- Can you it the other way around? 
-- I.e. rewrite the following expressions as list
-- comprehensions.

-- 1. map (+3) xs

hofunc7 xs = map (+3) xs

func7 xs = [ x+3 | x <- xs ]

-- 2. filter (>7) xs

hofunc8 xs = filter (>7) xs

func8 xs = [ x | x <- xs, x > 7 ]

-- 3. concat (map (\x -> map (\y -> (x,y)) ys) xs)

hofunc9 xs ys = concat (map (\x -> map (\y -> (x,y)) ys) xs)

func9 xs ys = [ (x,y) | x <- xs, y <- ys]

-- 4. filter (>3) (map (\(x,y) -> x+y) xys)

hofunc10 xys = filter (>3) (map (\(x,y) -> x+y) xys)

func10 xys = [ x+y | (x,y) <- xys, x+y > 3]