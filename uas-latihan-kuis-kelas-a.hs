-- Kuis 2 Pemrograman Fungsional Kelas A

--- 1) Given a list of words, remove from the list all those which contain four or more vowels
--- and those which have same letter appearing twice (or more) in a row. In addition, any word
--- containing numbers should have the numbers removed. Note that the number removal should happen
--- before any other operations so that the subsequent operations can remove the word if necessary.

--- weirdFilter :: [String] -> [String]
--- weirdFilter ["ab3c", "bananae", "fuzzy", "c1c2"]  
--- ["abc"]

notNumber x = x `notElem` "1234567890"
--- return true if element is a number, false otherwise.
--- Test not number using char (single quote). Don't use double quote, as it will change the type from character to string.

numberRemoval = filter notNumber
--- numberRemoval removes all number instances from a string.
--- To remove numbers in a list of strings, use map.

--- >>> numberRemoval "1234alya5678"
--- ["alya"]

--- (STEP 1)
--- >>> map numberRemoval ["ab3c", "bananae", "fuzzy", "c1c2"]  
--- ["abc","bananae","fuzzy","cc"]


isVowel x = x `elem` "aiueoAIUEO"
--- return true if element is a vowel, false otherwise.

countVowel = length . filter isVowel
--- countVowel essentially removes all non-vowels in a string (filter isVowel), then calculates its length
--- the . in "length . filter" is used to make a function composition.
--- length(filter isVowel("abc")) = (length . filter isVowel) "abc"

isThreeOrLessVowels str = countVowel str < 4
--- returns true if vowel is less than or equal to 3, false otherwise.
--- use (filter) to return true values only in a list.

--- >>> isThreeOrLessVowels "Top Haskell"
--- False

--- >>> filter isThreeOrLessVowels ["ab3c", "bananae", "fuzzy", "c1c2"]  
--- ["ab3c","fuzzy","c1c2"]

--- >>> map numberRemoval ["ab3c", "bananae", "fuzzy", "c1c2"]  
--- ["abc","bananae","fuzzy","cc"]

--- >>> filter isThreeOrLessVowels (map numberRemoval ["ab3c", "bananae", "fuzzy", "c1c2"])
--- ["abc","fuzzy","cc"] (STEP 2)

tempFunc2 = filter (isThreeOrLessVowels . numberRemoval)
--- filter (isThreeOrLessVowels(numberRemoval ("ab3c", "bTananae", "fuzzy", "c1c2")))
--- From first observation, seems like numberRemoval function is not executed, only filters vowels.


filterVowelsAndRemoveNumbers = ((filter isThreeOrLessVowels) . (map numberRemoval)) 
--- makes a composition function for step 2.
--- filter isThreeOrLessVowels (map numberRemoval ["ab3c", "bananae", "fuzzy", "c1c2"])

--- filterVowelsAndRemoveNumbers ["ab3c", "bananae", "fuzzy", "c1c2"]
--- ["abc","fuzzy","cc"] (STEP 3)

isNotAppearTwiceInARow (a:b:ls) | (a==b)   = False --- if there is a character a and b (rest of characters are represented as ls), which a equals b, then immediately returns false
isNotAppearTwiceInARow (a:ls)              = isNotAppearTwiceInARow ls --- recursive strategy: checks the remaining characters for duplication.
isNotAppearTwiceInARow []                  = True --- returns true if there are no letters that return twice in a row.

--- isNotAppearTwiceInARow "cool"
--- False

weirdFilter = filter isThreeOrLessVowels 
             . filter isNotAppearTwiceInARow 
             . map numberRemoval

--- uses function composition. Runs from the last function, and goes forward.
--- >>> weirdFilter ["ab3c", "bananae", "fuzzy", "c1c2"]
--- ["abc"]

and' p1 p2 = (\x -> p1 x && p2 x)
--- Returns true if both functions (p1 and p2) return true.

weirdFilter3 = filter (isThreeOrLessVowels `and'` isNotAppearTwiceInARow) . (map numberRemoval)
--- >>> weirdFilter3 ["ab3c", "bananae", "fuzzy", "c1c2"]
--- ["abc"]



--- 2) Write a function rotabc that changes a's to b's,
--- b's to c's and c's to a's in a string. Only lowercase
--- letters are affected. (source: https://www2.cs.arizona.edu/classes/cs372/spring14)

{-
rotabc (x:xs) | x == 'a' = 'b' : rotabc xs 
              | x == 'b' = 'c' : rotabc xs 
              | x == 'c' = 'a' : rotabc xs  
              | otherwise  = x : rotabc xs
-}

--- At first, (_) can't be compiled. (_) then is replaced with (otherwise)
-- When function is implemented using (otherwise), it becomes a non exhaustive pattern
-- because there cases that are not covered. i.e: No clear indication when the function will end.

rotabc' (x:xs) | x == 'a' = 'b' : rotabc xs 
              | x == 'b' = 'c' : rotabc xs 
              | x == 'c' = 'a' : rotabc xs  
              | otherwise  = x : rotabc xs
rotabc' [] = []

rotabc = map abc 
   where abc 'a' = 'b'
         abc 'b' = 'c'
         abc 'c' = 'a'
         abc  x  =  x


--- 3) Definisikan fungsi last, dengan menerapkan point-free style.
--- Fungsi last tersebut menerima sebuah list  dan mengembalikan elemen 
--- terakhir dari list tersebut.

last' = head . reverse
--- (head . reverse) x = head(reverse(x))



--- 4) Sebagaimana materi kuliah terkait Composing Contract, diperlihatkan sebuah
--- contract yang disebut Zero-Coupon Bound (zcb).

--- Pemanggilan fungsi zcb t x k,
--- menyatakan bahwa pada waktu t, contract ini akan senilai dengan x pada kurs k .
--- Misalkan fungsi contract definisi lain sudah tersedia. Bagaimana mengkomposisikan
--- nya untuk mendefinisikan fungsi zcb. Fungsi yang boleh anda gunakan adalah
--- antara lain: (when, give, and, or, at, scale, konst, one, zero)

--- zcb :: Date -> Double -> Currency -> Contract

--- ANSWER
--- zcb t x k = at t (scale x (one k))   maybe?

--- FUNCTIONS IN COMPOSING CONTRACT

{- 

[AND]
and :: Contract -> Contract -> Contract
-- Both c1 and c2

[GIVE]
give :: Contract -> Contract
-- invert role of parties

[ONE]
one :: Currency -> Contract
-- receive one unit of currency immediately

[SCALEK]
scaleK :: Float -> Contract -> Contract
-- Acquire specified number of contracts

[AT]
at :: Date -> Contract -> Contract
-- Acquire the contract at specified date

[WHEN]

[OR]
or :: Contract -> Contract -> Contract
-- Acquire either c1 or c2 immediately

[KONST]

[ZERO]
zero :: Contract
-- A worthless contract

[ANYTIME]
anytime :: Contract -> Contract
-- Acquire the underlying contract at any time before it expires (but must be acquired)

[TRUNCATE]
truncate :: Date -> Contract -> Contract
-- Truncate the horizon of a contract


-}

--- APPLICATIONS

{-

c1,c2,c3 :: Contract
c1 = zcb (date "1 Jan 2010") 100 Pounds
c2 = zcb (date "1 Jan 2010") 100 Pounds
c3 = and c1 c2


european :: Date -> Contract -> Contract
european t u = at t (u `or` zero)
--- at a particular date, you may choose to acquire an "underlying" contract, or to decline.


--- option to acquire 10 Microsoft shares, for $100, anytime between t1 and t2 years from now
shares = zero `or` (scaleK -100 (one Dollar) `and` scaleK 10 (one MSShare))

golden_handcuff = at t1 (anytime (truncate t2 shares))

-}