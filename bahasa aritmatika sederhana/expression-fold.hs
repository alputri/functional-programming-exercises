-- data Expr = Float | Float :+ Float | Float :- Float | Float :* Float | Float :/ Float
--     | V String | Let String Expr Expr
--     deriving Show

-- evaluate :: Expr -> Float
-- evaluate (n1 :+ n2) = foldl (+) 0 ([n1] ++ [n2])
-- evaluate (n1 :- n2) = foldr (-) 0 ([n1] ++ [n2])
-- evaluate (n1 :* n2) = foldl (*) 1 ([n1] ++ [n2])
-- evaluate (n1 :/ n2) = foldr (/) 1 ([n1] ++ [n2])
-- the function above does arithmetic operations using fold
-- functions.
-- Each Float element is appended into a list, and then operates
-- the arithmetic on the elements.

-- Latihan mengganti ekspresi dan evaluate menjadi higher order function
data Expr = C Float | Expr :+ Expr | Expr :- Expr | Expr :* Expr | Expr :/ Expr
    | V String | Let String Expr Expr
    deriving Show

foldExp (add, mult, sub, div) (C x) = x
foldExp (add, mult, sub, div) (x :+ y) = add (foldExp (add, mult, sub, div) x) (foldExp (add, mult, sub, div) y)
foldExp (add, mult, sub, div) (x :* y) = mult (foldExp (add, mult, sub, div) x) (foldExp (add, mult, sub, div) y)
foldExp (add, mult, sub, div) (x :- y) = sub (foldExp (add, mult, sub, div) x) (foldExp (add, mult, sub, div) y)
foldExp (add, mult, sub, div) (x :/ y) = div (foldExp (add, mult, sub, div) x) (foldExp (add, mult, sub, div) y)

newEval expr = foldExp (add, mult, sub, div) expr
      where add a b = a + b
            mult a b = a * b
            sub a b = a - b
            div a b = a `div` b

-- >>newEval (C 2 :+ C 4)
-- > 6
