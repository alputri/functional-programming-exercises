module Robot where
import Data.Array
import Data.List
import Control.Monad
import Control.Applicative
import System.IO
import Graphics.SOE


data Direction = North 
                | East 
                | South 
                | West 
                deriving (Eq,Show,Enum)

right :: Direction -> Direction
right d = toEnum (succ (fromEnum d) `mod` 4)

data RobotState = RobotState    { position :: Position , facing :: Direction
                                , pen :: Bool , color :: Color
                                , treasure :: [Position], pocket :: Int
                                } deriving Show

newtype Robot a = Robot (RobotState -> Grid -> Window -> IO (RobotState, a))
turnRight :: Robot ()
turnRight = updateState (\s -> s {facing = right (facing s)})
moven :: Int -> Robot ()
moven n = mapM_ (const move) [1..n]


-- spiral movement using for forloop
spiral = forLoop [1..20] action 
    where
        action = \i -> (turnRight >> moven i >> turnRight >> moven i)

-- zigzag with for loop
zigzag = forLoop [1..10] action
    where action = \i -> (moven 5 >> turnRight >> move >> turnRight
                            >> moven 5 >> turnLeft >> move >> turnLeft)