--  Exercises from chapter 9-10 of The Craft of Functional Programming



-- (1) Define the length function using map and sum

-- Create a list of 1's of the same length as the input list (with map),
-- then compute its sum (with sum).

-- variation 1
length' a = sum (map (const 1) a)

-- a = "lala"
-- (map (const 1) a) = [1,1,1,1]
-- sum (map (const 1) a) = sum [1,1,1,1] = 4.

-- variation 2
length3 l = sum (map (\x -> 1) l)

-- map (\x -> 1) l == map assign1 l where assign1 x = 1
-- map function above basically assigns 1 to each element on l

assign1 x = 1

-- variation 3 (??)
length2 = sum . map (const 1)

-- understanding function composition (.)
-- sumEuler = sum . (map euler) . mkList
-- is equivalent to
-- sumEuler x = sum (map euler (mkList x))

-- which means
-- length2 = sum . map (const 1)
-- is equivalent to
-- length2 x = sum (map (const1) x), which makes this identical to variation 1



-- (2) What does map (+1) (map (+1) xs) do? Can you conclude anything in
-- general about properties of map f (map g xs), where f and g are arbitrary
-- functions?

-- map (+1) (map (+1) xs) iterates through all elements in the list and adds
-- it by 1, twice. 

-- Example: map (+1) (map (+1) [1,3,5]). map (+1) [1,3,5] will run, iterating
-- through the list and adding it by 1, resulting in [2,4,6]. The next step
-- will run map +1 [2,4,6], resulting in [3,5,7].

-- map f (map g xs) can be done as long as f and g defines a function
-- that can be operated by elements inside a function,
-- otherwise it will produce an error.



-- (3) Give the type of, and define the function iter so that
-- iter n f x = f (f (... (f x)))
-- where f occurs n times on the right-hand side of the equation.

-- For instance, we should have iter 3 f x = f (f (f x))
-- and iter 0 f x should return x

iter :: Integral n => n -> (a -> a) -> a -> a

-- n is defined as an integral.
-- input has 3 parameters, Integral n, a function that
-- maps variable with data type a to another variable
-- with the same data type, and another variable with
-- data typ a.
-- Function will give an output with data type a.

iter 0 f x = x
iter n f x = iter (n-1) f (f x)

-- iter will run recursively, where it will stop when it arrives at
-- iter 0 f x.

-- Example: iter 1 (1+) 5
-- Step 1 -> iter 1 (1+) 5 = iter 0 (1+) (1+5)
-- Step 2 -> iter 0 (1+) (1+5) = (1+5)
-- = 6.



-- (4) What is the type and effect of the following function?
-- \n -> iter n succ

-- succ is the successor function, which increases a value by one:
-- Prelude> succ 33
-- 34

-- \n -> iter n succ is an anonymous function. The backslash is supposed
-- to express a lambda.
-- it will run the iter function by taking 2 parameters, n and the number
-- we want to be operated with succ.

-- Example: (\n -> iter n succ) 2 3, where it is equivalent to iter 2 succ 3
-- it will result in 5.



-- (5) *) How would you define the sum of the squares of the natural numbers 1
-- to n using map and foldr?

-- understanding foldr
-- Example: foldr (-) 54 [10, 11]
-- Starting accumulator = 54
-- 11 -   54  = -43
-- 10 - (-43) =  53

sumn :: (Eq t, Floating t, Enum t) => t -> t

sumn 0 = 1
sumn x = foldr (+) 0 (map (**2) [1..x])

-- alternative
sumSquares n = foldr (+) 0 (map (\x -> x*x) [1..n])



-- (6) How does the function
-- mystery xs = foldr (++) [] (map sing xs)
-- where
-- sing x = [x]
-- behave?

mystery xs = foldr (++) [] (map sing xs)
    where
        sing x = [x]

-- In essence, this function will move list items to a new list.

-- mystery xs takes a list as a parameter. Each element on the list
-- will be iterated using the map function, and each element will be
-- turned into a list that only contains the element itself. These elements
-- will be contained in a list, which we will refer to as x.
-- Foldr will be operated on list x, which will append each element into
-- a new list in the same order as the initial parameter. 



-- (7) If id is the polymorphic identity function, defined by id x = x,
-- explain the behavior of the expressions
-- (id . f) (f . id) (id f)
-- If f is of type Int -> Bool, at what instance of its most general type a ->
-- a is id used in each case?

f :: Int -> Bool
f 0 = False
f x = True

id' x = x

-- (id' . f) x will equal to id' (f x) but (id' . f 10) will produce error
-- (f . id') x will equal to f (id' x) or f x itself but (f . id' 10) will produce error
-- (id' f x) will equal to id' (f x)
-- id' will behave as its general type a -> a in (id' .f ) x and (id' f x)



-- (8) Define a function composeList which composes a list of functions into a single
-- function. You should give the type of composeList, and explain why the function
-- has this type. What is the effect of your function on an empty list of functions?

composeList :: [a -> a] -> (a -> a)

composeList []     = id
composeList (f:fs) = f . composeList fs



-- (9) Define the function
-- flip :: (a -> b -> c) -> (b -> a -> c)
-- which reverses the order in which its function argument takes its arguments.
-- The following example shows the effect of flip:
-- Prelude> flip div 3 100
-- 33

flip' :: (a -> b -> c) -> (b -> a -> c)
flip' f = \x y -> f y x

-- alternative: make y become operand
flip2' :: (a -> b -> c) -> (b -> a -> c)
flip2' x y z = z `y` x