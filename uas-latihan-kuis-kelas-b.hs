import Data.List 
import qualified Data.Char as Char

-- Kuis 2 Pemrograman Fungsional Kelas B

--- 1) Given a sentence, define a function called capitalise which returns
--- the same sentence with all words capitalised except the ones from a given list.
--- (source: https://www.fer.unizg.hr/)

--- capitalise :: String -> [String] -> String
--- capitalise "this is a sentence." ["a", "is"]
--- "This is a Sentence."


getWord word [] = (word,[])
getWord word (x:xs) | x == ' ' = (word, xs)
                    | x /= ' ' = getWord (word ++ [x]) xs

-- Gets first word in a sentence. Word stops when a space occurs.
-- >>> getWord [] "this is a sentence"
-- ("this","is a sentence")

splitter lw []  = lw
splitter lw inp = let (word, rest) = getWord "" inp
                   in splitter (lw ++ [word]) rest

-- >>> splitter [] "this is a sentence."
-- ["this","is","a","sentence."]
-- //understand more (let? in?)

upper el word = if word `elem` el 
                    then word 
                    else Char.toUpper (head word) : (tail word)

-- checks whether inputted word (word) is in list of words (el).
-- if true, word is not capitalized. if false, word is capitalized.

combine []  = []
combine [a] = a
combine (a:xs) = a ++ " " ++ (combine xs)

-- combines words in a list into sentence with a space to divide it.

-- >>> combine ["this","is","a","sentence."]
-- "this is a sentence."


capitalise1 inp el = let lw = splitter [] inp
                     in combine (map (upper el) lw)

capitalise2 inp el = combine (map (upper el) (splitter [] inp))


-- >>> capitalise1 "this is a sentence." ["a", "is"]
-- >>> capitalise2 "this is a sentence." ["a", "is"]
-- "This is a Sentence. "


-- Another implementation using function composition but flips the input order.
capitalise3 el  =  combine . (map (upper el)) . (splitter [])

-- >>> capitalise3 ["a", "is"] "this is a sentence." 
-- "This is a Sentence. "



--- 2) Write a definition of composition function (compose) similar to the compose/dot
--- operator ( • ) in Haskell prelude, that accept two functions and return a new
--- function. Define the type of the compose function.

compose :: (t1 -> t2) -> (t3 -> t1) -> t3 -> t2
compose a b = (\x -> a(b x))



--- 3) Definisikan fungsi last, dengan menerapkan foldr atau foldl.
--- Fungsi last tersebut menerima sebuah list  dan mengembalikan elemen 
--- terakhir dari list tersebut.

last' :: [a] -> a
last' = foldl (\_ x -> x) undefined